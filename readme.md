In this test, we will create a simple game called tic-tac-toe. Tic Tac Toe (also called Noughts and crosses, Xs and Os, XOX Game, 3 in a row) is a very popular children’s pencil and paper board game, which is often played and enjoyed by many adults, as well. 

Two players can play with each other by using a 3×3 board in Tic Tac Toe. One player chooses noughts (O), the opposing player uses crosses(X) and the first player to align 3 of their identical symbols, either noughts or crosses, (horizontally, vertically or diagonally) wins the game.

# Project Structure

You are given the following project structure:

```
|-public         <- The content of your game website.
|  |-script      <- The script folder stores JavaScript.
|  |  |-game.js  <- The only script file of this project.
|  |                Please note that the JavaScript will run in the browser
|  |                directly. So some of the advanced ES6 features may not
|  |                work properly. We will use the latest Chrome or Firefox
|  |                browser to open the site.
|  |
|  |-styles      <- The styles folder stores CSS files.
|  |  |-main.css <- The only CSS file of this project.
|  |  |-normalize.css <- The CSS default start.
|  |
|  |-index.html  <- The only HTML page of this project.
|
|-package.json   <- This package.json will be used to run the HTTP server
                    which hosts the website.
```

> **IMPORTANT!** **NO** 3rd party library is allowed.

> **IMPORTANT!** You should **NOT** create new file.

# How To Run The Website

You can use the following command to run the website:

```bash
$ npm install
$ npm run start
```

The website will be run on http://localhost:4444 by default.

You can use the `Ctrl+C` shortcut to stop the website from the command line.

**IMPORTANT**: Do not forget to use lint to ensure you coding style.

```
$ npm run lint
```

# Stories

All the ACs should be implemented accurately. Before we start. Let's watch the final result.

<img src="doc/final-result.gif"/>

## Story 1: 

### AC 1 Static Layout

Please complete the following static layout. The font family is 

```
Arial, Helvetica, sans-serif
```

And the whole structure should be **horizontally and vertically centered**.

<img src="doc/static-layout.jpg"/>

### AC 2 Hover effect

When the mouse hover on the board, the background color should turned into `lightgrey`. When the mouse is down, the background color should turned into `darkgrey`.

<img src="doc/mouse-click.gif"/>

### AC 3 Lay down the symbol

When the mouse click on the board. The "X" and "O" symbol will be laid down alternatively.

<img src="doc/lay-symbol.gif">

Determine the winner is out of the scope of this AC.

### AC 4 Hint on the next symbol

We should display a hint, telling the player what is the next symbol. The first symbol should be "X", then "O", then "X". And the format of the hint is:

*Next round: {symbol}*

Please check the mockup.

<img src="doc/hint.gif">

### AC 5 Determine the winner

We should display the winner or the final result immediately.

* If either side wins, then the result should be: *Congratulations, {symbol} wins*. (e.g. Congratulations, X wins)
* If the game ends as a tie (nobody wins). The result should be: *The game ends as a tie.*
* Otherwise the result should be: *Playing ^_^*.

### AC 6 Prevent adding additional symbols

The player should not add symbols on the board in the following conditions:

* When the symbol already exists.
* When the game is finished (One of the player wins, or the game ends as a tie).

<img src="doc/preventing-operation.gif">

### AC 7 Reset all the game when clicking "Reset" button

The game should be reset when "Reset" button is clicked.

<img src="doc/reset.gif">